# baekjoon  
## For Baekjoon Online Judge Problem Source codes *2018.09.15* created

* [acmicpc practice problem set](https://www.acmicpc.net)  
* compiled by [online_gdb](https://www.onlinegdb.com)  
* [kakao blind recruitment 2017 problem set](https://programmers.co.kr/learn/challenges)
* [kakao blind recruitment 2018 problem set](https://programmers.co.kr/learn/challenges?tab=all_challenges&challenge_collection_part_ids[]=12286)
* _You **can** combine them_  

##problem solved
9653

## To read later  
[mastering-markdown](https://guides.github.com/features/mastering-markdown/)  
[wikipedia_en](https://en.wikipedia.org/wiki/Markdown)  
[werewolf problem](http://wcipeg.com/problem/ccc14s2p3)  
[werewolf problem solution](http://amugelab.tistory.com/entry/CCC14-Stage-2-Werewolf)  
[git](https://blog.naver.com/ijoos/220980059526)  
___  
> For beautiful eyes, look for the good in others; for beautifullips, speak only words of kindness; and for poise, walk with the knowledge that you are never alone.

Inline <abbr> title="creation date">2018.09.15_created </abbr>  
Inline <abbr title="Hypertext Markup Language">HTML</abbr> is supported.
___  
