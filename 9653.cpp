#include <iostream>
#include <string>
#include <queue>
#include <stdio.h>
using namespace std;

int dx[] = {-1,1,0,0,0,0};
int dy[] = {0,0,-1,1,0,0};
int dz[] = {0,0,0,0,1,-1};

typedef struct point{
    
    int x;
    int y;
    int z;
    
    point(int x_, int y_, int z_) :x(x_),y(y_),z(z_) 
    {
        
    }
    
    point() // needed
    {
        
    }
    
}point;

bool operator==(const point& lhs,const point&rhs){
    return lhs.x==rhs.x&&lhs.y==rhs.y&&lhs.z==rhs.z;
}

ostream& operator<<(ostream& os, const point& p){
    os <<p.x<<" "<<p.y<<" "<<p.z<<endl;
    return os;
}

char auaicn[40][40][40];

int main(int argc, const char** argv){
    int L,R,C;
    point start,end;
    
    while(true){
        
        cin >>L>>R>>C;
        
        if(!(L*R*C))
                break;
                
        // initialize
        for (int i=0;i<40;i++)
                for (int j=0;j<40;j++)
                    for (int k=0;k<40;k++)
                        auaicn[i][j][k]='#';
                        
        for (int i=0;i<L;i++){
            for (int j=0;j<R;j++){
                for (int k=0;k<C;k++){
                  
                    cin >>auaicn[i+1][j+1][k+1];
                    switch(auaicn[i+1][j+1][k+1]){
                        case 'S':
                            start = point(i+1,j+1,k+1);
                            break;
                        case 'E':
                            end  = point(i+1,j+1,k+1);
                            break;
                    }
                }
            }
        }
        
        int result=-1;
        queue<pair<point,int>> q;
        
        q.push({start,0});
        auaicn[start.x][start.y][start.z] = '\0';
        int k;
        while(!q.empty()){
            
            point pos = q.front().first;
            int time_ = q.front().second;
            
            if(pos == end){
                result = time_;
                
                break;
            }
            
            for (int i=0;i<6;i++){
                int x_ = pos.x+dx[i];
                int y_ = pos.y+dy[i];
                int z_ = pos.z+dz[i];
                if(auaicn[x_][y_][z_]=='.'||auaicn[x_][y_][z_]=='E'){
                    q.push({point(x_,y_,z_),time_+1});
                    auaicn[x_][y_][z_]='\0';
                }
                
            }
            q.pop();
        }
        
        if(result!=-1)
            cout <<"Escaped in "<<result <<" minute(s)."<<endl;
        else
            cout <<"Trapped!"<<endl;
        //fflush(stdout);
            
    }
    return 0;
}